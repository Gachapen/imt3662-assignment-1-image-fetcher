package com.gachapen.hig.mobile.imagefetch;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

/**
 * An adapter used to represent {@link UrlFetch} auto complete values.
 * The values contained in this adapter is always represented by the values list
 * given in the constructor. If that list is changed, notifyDataSetChanged() has
 * to be called on this adapter to update its representation.
 * @author Magnus Bjerke Vik
 */
class AutocompleteAdapter extends BaseAdapter implements Filterable {
    private int layoutResource;
    private Filter autocompleteFilter;
    private LayoutInflater inflater;
    private Context context;
    
    /** All the items of this adapter not filtered. */
    private List<UrlFetch> itemList;
    
    /** 
     * The items this adapter will present the user of this adapter with.
     * These are filtered by the filtering class
     */
    private List<UrlFetch> filteredItemList;
    
    public AutocompleteAdapter(Context context, int layoutResource, List<UrlFetch> values) {
        super();
        this.context = context;
        this.layoutResource = layoutResource;
        this.itemList = values;
        this.filteredItemList = this.itemList;
        this.autocompleteFilter = new AutocompleteFilter();
        this.inflater = LayoutInflater.from(this.context);
    }
    
    @Override
    public int getCount() {
        return this.filteredItemList.size();
    }
    
    @Override
    public UrlFetch getItem(int position) {
        return this.filteredItemList.get(position);
    }
    
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    /**
     * Returns a {@link TextView} representing the URL of the UrlFetch at the provided
     * position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView view;
        
        if (convertView == null) {
            view = (TextView) this.inflater.inflate(this.layoutResource, parent, false);
        } else {
            view = (TextView) convertView;
        }
        
        view.setText(getItem(position).getUrl());
        return view;
    }

    @Override
    public Filter getFilter() {
        return this.autocompleteFilter;
    }

    /**
     * Filter which will filter all UrlFetch instances which have a URL containing a
     * substring of the constraint.
     * @author Magnus Bjerke Vik
     */
    private class AutocompleteFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults result = new FilterResults();
            List<UrlFetch> acceptedValues = new ArrayList<UrlFetch>();
            
            if(constraint != null && constraint.toString().length() > 0) {
                for (UrlFetch autocomplete : AutocompleteAdapter.this.itemList) {
                    boolean accepted = autocomplete.getUrl().toLowerCase().contains(constraint.toString().toLowerCase());
                    if (accepted == true) {
                       acceptedValues.add(autocomplete);
                    }
                }
            }
            
            result.count = acceptedValues.size();
            result.values = acceptedValues;
            return result;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            AutocompleteAdapter.this.filteredItemList = (List<UrlFetch>) results.values;
            if (results.count > 0) {
                notifyDataSetChanged();
            } else {
                notifyDataSetInvalidated();
            }
        }
    }
}
