package com.gachapen.hig.mobile.imagefetch;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * A representation of an Image downloaded from a webpage which can be stored
 * or loaded from storage.
 * @author Magnus Bjerke Vik
 */
class Image {
    private long id;
    private Bitmap bitmap;
    private String url;
    private String filename;
    
    /** The Context of this Image which will be used to save and load the bitmap. */
    private Context appContext;
    
    /** The UrlFetch instance representing the web page this Image was downloaded from. **/
    private UrlFetch urlFetch;
    
    /** Regex used to remove potentially harmful characters from the filename. */
    private static final String FILENAME_REPLACE_REGEX = "[|\\\\?*<\":>+\\[\\]/']";
    
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_FETCH_URL_ID = "fetch_url_id";
    private static final String COLUMN_IMAGE_URL = "image_url";
    private static final String COLUMN_FILENAME = "filename";
    
    public static final String TABLE_NAME = "image_info";
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
            + COLUMN_ID + " INTEGER PRIMARY KEY,"
            + COLUMN_FETCH_URL_ID + " INTEGER,"
            + COLUMN_IMAGE_URL + " TEXT,"
            + COLUMN_FILENAME + " TEXT"
            + ");";

    public Image(UrlFetch fetchUrl, Bitmap bitmap, String url, Context appContext) {
        this.id = -1;
        this.bitmap = bitmap;
        this.url = url;
        this.filename = this.url.replaceAll(FILENAME_REPLACE_REGEX, "");
        this.urlFetch = fetchUrl;
        this.appContext = appContext;
    }
    
    /**
     * Check if the Image is stored on the device.
     * @return true if it is in the DB and the bitmap is stored. false if not.
     */
    public boolean isStored() {
        if (this.id == -1) {
            return false;
        }
        
        File bitmapFile = getCacheFile();
        if (bitmapFile.exists() == false) {
            return false;
        }
        
        return true;
    }
    
    public long getId() {
        return this.id;
    }
    
    public Bitmap getBitmap() {
        return bitmap;
    }

    public String getUrl() {
        return this.url;
    }

    /**
     * @return The file name used when storing the bitmap.
     */
    public String getFileName() {
        return filename;
    }
    
    /**
     * @return The file path to the stored bitmap.
     */
    public String getStoredFilePath() {
        File bitmapFile = getCacheFile();
        return bitmapFile.getPath();
    }
    
    /**
     * Save the Image to the device.
     * @return false if it already is stored, otherwise true.
     * @throws IOException if it failed saving the bitmap.
     */
    public boolean save(SQLiteOpenHelper dbHelper) throws IOException {
        final boolean alreadInDatabase = (this.id > -1);
        if (alreadInDatabase) {
            return false;
        }
        
        try {
            saveBitmap();
        } catch (IOException e) {
            throw new IOException("Could not save bitmap");
        }
        
        insertInDb(dbHelper);
        
        return true;
    }
    
    /**
     * Removes the Image from the storage. This includes
     * the DB row and the bitmap.
     * @return true, stupididly.
     */
    public boolean remove(SQLiteOpenHelper dbHelper) {
        deleteBitmap();
        deleteFromDb(dbHelper);
        
        return true;
    }
    
    /**
     * Load the {@link Bitmap} of this instance. This can the be retrieved with
     * getBitmap().
     * @throws IOException if it couldn't be loaded.
     */
    public void loadBitmap() throws IOException {
        File bitmapFile = getCacheFile();
        if (bitmapFile.exists() == false) {
            throw new IOException("File does not exist");
        }
        
        this.bitmap = loadBitmap(bitmapFile.getPath(), this.appContext);
    }
    
    /**
     * Load only the {@link Bitmap} from a specified path.
     * @param path The path of the bitmap.
     * @return The {@link Bitmap} loaded.
     * @throws IOException if it failed reading.
     */
    public static Bitmap loadBitmap(String path, Context context) throws IOException {
        final int BUFFER_SIZE = 256 * 1024; // 256 KiB large buffer.
        byte[] byteBuffer = new byte[BUFFER_SIZE];
        byte[] byteArray = null;
        
        File bitmapFile = new File(path);
        
        BufferedInputStream inputStream = null;
        
        try {
            inputStream = new BufferedInputStream(new FileInputStream(bitmapFile));
            
            int bytesRead = 0;
            bytesRead = inputStream.read(byteBuffer, 0, BUFFER_SIZE);
            while (bytesRead != -1) {
                byte[] oldByteArray = byteArray;
                
                int oldByteArrayLength = 0;
                if (oldByteArray != null) {
                    oldByteArrayLength = oldByteArray.length;
                }
    
                byteArray = new byte[oldByteArrayLength + bytesRead];
                
                if (oldByteArray != null) {
                    System.arraycopy(oldByteArray, 0, byteArray, 0, oldByteArray.length);
                }
                
                System.arraycopy(byteBuffer, 0, byteArray, oldByteArrayLength, bytesRead);
                
                bytesRead = inputStream.read(byteBuffer, 0, BUFFER_SIZE);
            }
        } catch (IOException e) {
            throw e;
        } finally {
            inputStream.close();
        }
        
        if (byteArray == null || byteArray.length <= 0) {
            throw new IOException("Read zero bytes from " + path);
        }
        
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
    }
    
    /**
     * Get the an instance of the Image with the specified url.
     * @param url The URL used to find the image.
     * @return The instance of the Image, or null if not found.
     */
    public static Image get(String url, final SQLiteOpenHelper dbHelper, final Context appContext) {
        synchronized (dbHelper) {
            final SQLiteDatabase db = dbHelper.getReadableDatabase();
            final String whereClause = "" + COLUMN_IMAGE_URL + " = '" + url + "'";
            final Cursor entryCursor = db.query(
                    TABLE_NAME, 
                    new String[] { COLUMN_ID, COLUMN_FETCH_URL_ID, COLUMN_IMAGE_URL, COLUMN_FILENAME }, 
                    whereClause, 
                    null, 
                    null, 
                    null, 
                    null
            );
            
            Image image = null;
            
            entryCursor.moveToFirst();
            if (!entryCursor.isAfterLast()) {
                image = cursorToImage(entryCursor, dbHelper, appContext);
            }
            
            entryCursor.close();
            db.close();
            
            return image;
        }
    }
    
    /**
     * Get all the Image instances fetched from the specified UrlFetch.
     * @param fromUrlFetch The UrlFetch used to fetch these images. The id of the UrlFetch is used.
     * @return A list of the images found.
     */
    public static List<Image> getAll(UrlFetch fromUrlFetch, final SQLiteOpenHelper dbHelper, final Context appContext) {
        final List<Image> imageList = new ArrayList<Image>();
        
        synchronized (dbHelper) {
            final SQLiteDatabase db = dbHelper.getReadableDatabase();
            final String whereClause = COLUMN_FETCH_URL_ID + " = " + fromUrlFetch.getId();
            final Cursor entryCursor = db.query(
                    TABLE_NAME, 
                    new String[] { COLUMN_ID, COLUMN_FETCH_URL_ID, COLUMN_IMAGE_URL, COLUMN_FILENAME }, 
                    whereClause, 
                    null, 
                    null, 
                    null, 
                    null
            );
            
            entryCursor.moveToFirst();
            while (!entryCursor.isAfterLast()) {
                final Image image = cursorToImage(entryCursor, dbHelper, appContext);
                imageList.add(image);
                entryCursor.moveToNext();
            }
            
            entryCursor.close();
            db.close();
        }
        
        return imageList;
    }
    
    /**
     * Get all the URLs of the Images fetched from the specified UrlFetch.
     * This is practically the same as getAll(), but will return a list containing only the URLs
     * of the images.
     * @param fromUrlFetch The UrlFetch used to fetch these images. The id of the UrlFetch is used.
     * @return A list of Strings containing the URLs.
     */
    public static List<String> getAllImageUrls(UrlFetch fromUrlFetch, final SQLiteOpenHelper dbHelper) {
        final List<String> urlList = new ArrayList<String>();
        
        synchronized (dbHelper) {
            final SQLiteDatabase db = dbHelper.getReadableDatabase();
            final String whereClause = COLUMN_FETCH_URL_ID + " = " + fromUrlFetch.getId();
            final Cursor entryCursor = db.query(
                    TABLE_NAME, 
                    new String[] { COLUMN_IMAGE_URL }, 
                    whereClause, 
                    null, 
                    null, 
                    null, 
                    null
            );
            
            entryCursor.moveToFirst();
            while (!entryCursor.isAfterLast()) {
                urlList.add(entryCursor.getString(entryCursor.getColumnIndex(COLUMN_IMAGE_URL)));
                entryCursor.moveToNext();
            }
            
            entryCursor.close();
            db.close();
        }
        
        return urlList;
    }
    
    private Image() {
        this.id = -1;
        this.bitmap = null;
        this.url = null;
        this.filename = null;
        this.urlFetch = null;
        this.appContext = null;
    }
    
    /**
     * Insert the Image data into a new row in the DB.
     * @return true if it was inserted, othwise false.
     */
    private boolean insertInDb(SQLiteOpenHelper dbHelper) {
        final ContentValues values = new ContentValues();
        values.put(COLUMN_FETCH_URL_ID, this.urlFetch.getId());
        values.put(COLUMN_IMAGE_URL, this.url);
        values.put(COLUMN_FILENAME, this.filename);
        
        long newId = -1;
        
        synchronized (dbHelper) {
            final SQLiteDatabase db = dbHelper.getWritableDatabase();
            newId = db.insert(TABLE_NAME, null, values);
            db.close();
        }
        
        if (newId == -1) {
            return false;
        }
        
        this.id = newId;
        return true;
    }
    
    /**
     * Save the image {@link Bitmap} to the storage.
     * @return true.
     * @throws IOException if it failed saving.
     */
    private boolean saveBitmap() throws IOException {
        File bitmapFile = getCacheFile();
        BufferedOutputStream outStream = null;
        
        try {
            outStream = new BufferedOutputStream(new FileOutputStream(bitmapFile));
            this.bitmap.compress(Bitmap.CompressFormat.PNG, 0, outStream);
        } catch (IOException e) {
            throw e;
        } finally {
            if (outStream != null) {
                outStream.close();
            }
        }
        
        return true;
    }
    
    /**
     * Delete the bitmap from the storage if it is there.
     * @return true if it was successfully deleted, otherwise false.
     */
    private boolean deleteBitmap() {
        return getCacheFile().delete();
    }
    
    /**
     * Delete the row containing the data of this Image from the DB.
     * @return true if it was deleted, otherwise false.
     */
    private boolean deleteFromDb(SQLiteOpenHelper dbHelper) {
        int numDeletedRows = 0;
        
        synchronized (dbHelper) {
            final SQLiteDatabase db = dbHelper.getWritableDatabase();
            final String whereClause = "" + COLUMN_ID + " = " + this.id;
            numDeletedRows = db.delete(TABLE_NAME, whereClause, null);
            db.close();
        }
        
        if (numDeletedRows < 1) {
            return false;
        }
        
        this.id = -1;
        return true;
    }
    
    /**
     * Get the {@link File} representation of the cached {@link Bitmap}.
     * @return The {@link File} for the cache {@link Bitmap}.
     */
    private File getCacheFile() {
        File cacheDir = this.appContext.getExternalCacheDir();
        cacheDir.mkdirs();
        File bitmapFile = new File(cacheDir, this.filename);
        return bitmapFile;
    }
    
    /**
     * Convert a {@link Cursor} to an {@link Image} with the values from the {@link Cursor}.
     * @return The converted Image.
     */
    private static Image cursorToImage(Cursor cursor, SQLiteOpenHelper dbHelper, Context appContext) {
        final Image image = new Image();
        image.appContext = appContext;
        
        int urlFetchId = cursor.getInt(cursor.getColumnIndex(COLUMN_FETCH_URL_ID));
        image.urlFetch = UrlFetch.get(urlFetchId, dbHelper);
        
        image.id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
        image.url = cursor.getString(cursor.getColumnIndex(COLUMN_IMAGE_URL));
        image.filename = cursor.getString(cursor.getColumnIndex(COLUMN_FILENAME));
        
        return image;
    }
}
