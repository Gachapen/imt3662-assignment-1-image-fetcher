package com.gachapen.hig.mobile.imagefetch;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

/**
 * Task saving a {@link Bitmap} to the device so the user can access it.
 * @author Magnus Bjerke Vik
 */
class SaveImageToDeviceTask extends AsyncTask<Void, Void, String> {
    private final String IMAGE_SAVE_SUB_DIR = "ImageFetcher";
    private final String IMAGE_MIME_TYPE = "image/png";
    private final String IMAGE_FILE_PREFIX = "image_fetch_";
    private Random randomGenerator = new Random(System.currentTimeMillis());
    private Context context;
    private String filename;
    private Bitmap bitmap;
    private NotificationManager notificationManager;

    public SaveImageToDeviceTask(Context context, Bitmap bitmap, NotificationManager notificationMngr) {
        this.context = context;
        this.bitmap = bitmap;
        this.notificationManager = notificationMngr;
        
        SecureRandom random = new SecureRandom();
        String randomString = new BigInteger(130, random).toString(32); // I don't really know how this works, but I need a random alphanumeric string.
        this.filename = this.IMAGE_FILE_PREFIX + randomString + ".png";
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result != null) {
            showNotification(result);
        } else {
            Toast.makeText(this.context, "Could not save image.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected String doInBackground(Void... params) {
        File picturesDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File imageDirectory = new File(picturesDirectory, this.IMAGE_SAVE_SUB_DIR);
        imageDirectory.mkdirs();

        File imageFile = new File(imageDirectory, this.filename);

        BufferedOutputStream fileOutput = null;

        try {
            fileOutput = new BufferedOutputStream(new FileOutputStream(imageFile));
            this.bitmap.compress(Bitmap.CompressFormat.PNG, 0, fileOutput);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (fileOutput != null) {
                try {
                    fileOutput.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        // Make sure it pops up in the gallery.
        MediaScannerConnection.scanFile(
                this.context,
                new String[] { imageFile.getPath() },
                new String[] { this.IMAGE_MIME_TYPE },
                null
        );

        return "file://" + imageFile.getPath();
    }

    /**
     * Show a notification saying that the image was saved.
     * If the user presses this it will open in their image viewing app.
     * @param filePath The path of the file that will be opened.
     */
    private void showNotification(String filePath) {
        Intent notificationIntent = new Intent();
        notificationIntent.setAction(Intent.ACTION_VIEW);
        Uri imageUri = Uri.parse(filePath);
        notificationIntent.setDataAndType(imageUri, this.IMAGE_MIME_TYPE);

        // TODO: The documentation says that using this with implicit intents
        // are unsafe, but how should I do it then?
        PendingIntent pendingIntent = PendingIntent.getActivity(this.context, 0, notificationIntent, 0);

        NotificationCompat.Builder notificationBuilder = 
                new NotificationCompat.Builder(this.context)
                .setContentTitle("Image saved")
                .setContentText("Touch to open.")
                .setTicker("Image saved")
                .setSmallIcon(R.drawable.content_save)
                .setLargeIcon(this.bitmap)
                .setStyle(new NotificationCompat.BigPictureStyle().bigPicture(this.bitmap))
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        Notification notification = notificationBuilder.build();

        // Random id so notifications won't kill each other.
        this.notificationManager.notify(this.randomGenerator.nextInt(), notification);
    }
}
