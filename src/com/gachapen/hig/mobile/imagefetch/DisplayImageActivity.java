package com.gachapen.hig.mobile.imagefetch;

import java.io.IOException;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * Displays a single image.
 * @author Magnus Bjerke Vik
 */
public class DisplayImageActivity extends Activity {
    private Bitmap imageBitmap = null;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.activity_display_image);

	    Intent intent = getIntent();
	    
        String bitmapPath = intent.getStringExtra("bitmap_path");
        if (bitmapPath == null) {
            Toast.makeText(this, "Could not show image", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        
        try {
            this.imageBitmap = Image.loadBitmap(bitmapPath, getApplicationContext());
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Could not show image", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
	    
	    ImageView imageView = (ImageView) findViewById(R.id.image);
	    imageView.setImageBitmap(this.imageBitmap);
	    
	    String imageUrl = intent.getStringExtra("url");
	    if (imageUrl != null) {
	        setTitle(imageUrl);
	    }
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            // I don't quite understand this yet, but it is needed for
            // making sure the up navigation button doesn't unnecessary
            // recreate the main activity.
            Intent upIntent = new Intent(this, MainActivity.class);
            if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                NavUtils.navigateUpTo(this, upIntent);
                finish();
            } else {
                finish();
            }
            return true;
        case R.id.action_save_image:
            if (canStoreFiles() == true) {
                saveImage();
            } else {
                Toast.makeText(this, "Can't access storage.", Toast.LENGTH_SHORT).show();
            }
        default:
            return super.onOptionsItemSelected(item);
        }
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.display_image, menu);
        return true;
    }
    
    /**
     * Save the image to the users external device so they can access it..
     */
    private void saveImage() {
        Toast.makeText(this, "Saving image.", Toast.LENGTH_SHORT).show();
        SaveImageToDeviceTask saveTask = new SaveImageToDeviceTask(
                getApplicationContext(),
                this.imageBitmap,
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE)
        );
        saveTask.execute();
    }

    /**
     * @return true if it can store files, false if not.
     */
    private boolean canStoreFiles() {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            return true;
        }
        
        return false;
    }
}
