package com.gachapen.hig.mobile.imagefetch;

import com.gachapen.hig.mobile.imagefetch.R;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.widget.Toast;

/**
 * The only job of this activity is to forward input from {@link UrlInputFragment}
 * to {@link ImagesViewFragment}.
 * @author Magnus Bjerke Vik
 */
public class MainActivity extends FragmentActivity implements UrlInputFragment.InputSetListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

	@Override
    public void onInputSet(UrlFetch urlFetch) {
		ConnectivityManager connManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo networkInfo = connManager.getActiveNetworkInfo();
    	
    	if (networkInfo != null && networkInfo.isConnected()) {
    		ImagesViewFragment viewFragment = (ImagesViewFragment) getSupportFragmentManager().findFragmentById(R.id.imageview_fragment);
    		viewFragment.downloadImages(urlFetch);
    	} else {
    		Toast.makeText(getApplicationContext(), R.string.no_connection, Toast.LENGTH_SHORT).show();
    	}
	    
    }	
}
