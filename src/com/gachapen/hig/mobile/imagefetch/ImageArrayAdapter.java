package com.gachapen.hig.mobile.imagefetch;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

/**
 * An array adapter used to contain {@link Image}s. The view returned by this
 * will be an {@link ImageView} with the {@link Bitmap} from the {@link Image}.
 * @author Magnus Bjerke Vik
 *
 */
class ImageArrayAdapter extends ArrayAdapter<Image> {
	private int layoutResource;
	private LayoutInflater inflater;
	
	/**
	 * Creates a new adapter instance.
	 * @param context The context of this adapter.
	 * @param resource The resource used to create the {@link ImageView} for an {@link Image}.
	 */
	public ImageArrayAdapter(Context context, int resource) {
	    super(context, resource);
	    this.layoutResource = resource;
	    this.inflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

	/**
	 * Returns an {@link ImageView} with the {@link Bitmap} of the {@link Image} at the position
	 * given.
	 */
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView view = null;
        
        if (convertView == null) {
            view = (ImageView) inflater.inflate(this.layoutResource, parent, false);
        } else {
            view = (ImageView) convertView;
        }

        view.setImageBitmap(getItem(position).getBitmap());
        return view;
    }
}