package com.gachapen.hig.mobile.imagefetch;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

// TODO: Limit cache size and clean it.
/**
 * A fragment displaying the images downloaded, or a progressbar
 * while downloading.
 * @author Magnus Bjerke Vik
 *
 */
public class ImagesViewFragment extends Fragment {
    final static String TAG = "ImageViewFragment";
    
	private ImageArrayAdapter imageArrayAdapter = null;
	private GridView imageGrid = null;
	private ProgressBar imageLoadProgress = null;
	
	/** The URL of the web page the images loaded were downloaded from. */
	private UrlFetch webpageUrl = null;
	
	private DatabaseHelper dbHelper = null;
	
	private LoadImagesTask loadImagesTask = null;
	private DownloadImagesTask downloadImagesTask = null;
	
	/** If the web page is currently being downloaded and parsed. */
	private boolean downloadingWebPage = false;
	
	private Context applicationContext = null;
	
	@Override
	public void onAttach(Activity activity) {
	    super.onAttach(activity);
	    this.applicationContext = activity.getApplicationContext();
	}
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    // We want this fragment to be retained during activity destruction so we
	    // don't have to reload the images.
	    setRetainInstance(true);
	    
	    this.imageArrayAdapter = new ImageArrayAdapter(getActivity(), R.layout.image_grid_item);
	    this.dbHelper = new DatabaseHelper(getActivity());
	    this.webpageUrl = UrlFetch.getLatest(this.dbHelper);
	    this.loadImagesTask = new LoadImagesTask();
	    this.loadImagesTask.execute(this.webpageUrl);
    }
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View fragmentView = inflater.inflate(R.layout.fragment_images_view, container, false); // Set the layout.
        
        this.imageGrid = (GridView) fragmentView.findViewById(R.id.image_grid);
        this.imageGrid.setAdapter(this.imageArrayAdapter);
        this.imageGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
	            Image imageClicked = ImagesViewFragment.this.imageArrayAdapter.getItem(position);
	            openImageInActivity(imageClicked);
            }
		});
        this.imageGrid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Image imageClicked = ImagesViewFragment.this.imageArrayAdapter.getItem(position);

                Toast.makeText(getActivity(), "Saving image.", Toast.LENGTH_SHORT).show();
                
                SaveImageToDeviceTask saveTask = new SaveImageToDeviceTask(
                        ImagesViewFragment.this.applicationContext, 
                        imageClicked.getBitmap(),
                        (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE)
                );
                saveTask.execute();
                
                return true;
            }
        });
  
        this.imageLoadProgress = (ProgressBar) fragmentView.findViewById(R.id.load_webpage_progress);
        
		return fragmentView;
	}
	
	@Override
    public void onStart() {
        super.onStart();
        
        // In case this fragment was restarted (without destruction) while downloading the web page
        // we need to explicitly show the loading progress again.
        if (this.downloadingWebPage == true) {
            showLoadingProgress();
        }
    }

    @Override
	public void onDestroyView() {
	    super.onDestroyView();
	}
	
	@Override
    public void onDestroy() {
	    // Cancel the loading/downloading tasks since we don't need them anymore.
        if (this.downloadImagesTask != null) {
            this.downloadImagesTask.cancel(true);
        }
        if (this.loadImagesTask != null) {
            this.loadImagesTask.cancel(true);
        }
        
        super.onDestroy();
    }

	/**
	 * Download and show images from the web page provided by the UrlFetch.
	 * @param urlFetch The web page to download from.
	 */
    public void downloadImages(UrlFetch urlFetch) {
        // Cancel the loading/downloading tasks since we are going to download new images.
        if (this.downloadImagesTask != null) {
            this.downloadImagesTask.cancel(true);
        }
        if (this.loadImagesTask != null) {
            this.loadImagesTask.cancel(true);
        }
        
	    this.webpageUrl = urlFetch;
	    this.imageArrayAdapter.clear();
	    this.downloadImagesTask = new DownloadImagesTask();
	    this.downloadImagesTask.execute(urlFetch.getUrl());
		Log.d(TAG, "Downloading " + urlFetch.getUrl());
	}
	
    /**
     * Opens the provided {@link Image} in the Activity used to display a single image.
     * @param image
     */
	private void openImageInActivity(Image image) {
	    Intent showImageIntent = new Intent(getActivity(), DisplayImageActivity.class);
	    showImageIntent.putExtra("filename", image.getFileName());
	    showImageIntent.putExtra("url", image.getUrl());

	    // We store the image since it might be too large for an intent.
        if (image.isStored() == false) {
            try {
                image.save(this.dbHelper);
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Could not open image.", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        
        showImageIntent.putExtra("bitmap_path", image.getStoredFilePath());
	    
        startActivity(showImageIntent);
	}
    
    private void showLoadingProgress() {
    	this.imageLoadProgress.setVisibility(View.VISIBLE);
    	this.imageGrid.setVisibility(View.GONE);
    }
    
    private void showImages() {
    	this.imageLoadProgress.setVisibility(View.GONE);
    	this.imageGrid.setVisibility(View.VISIBLE);
    }
	
    /**
     * Download the provided URL.
     * @param urlString The URL to download.
     * @return The inputStream representing the download.
     * @throws IOException if it could not download.
     */
	private HttpURLConnection connectToUrl(String urlString) throws IOException {
		URL url = new URL(urlString);
		
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setConnectTimeout(10000);
		connection.setReadTimeout(5000);
		connection.setRequestMethod("GET");
		connection.setInstanceFollowRedirects(true);
		
		connection.connect();
		int responseCode = connection.getResponseCode();
		Log.d(TAG, "Got response code " + responseCode);
		
		if (responseCode == HttpURLConnection.HTTP_OK) {
			return connection;
		} else {
		    throw new IOException("Response was " + responseCode + " when it should be 200 (HTTP_OK)");
		}
	}
    
	/**
	 * Downloads the URL and returns a String representing it.
	 * @param urlString The URL to download.
	 * @return A String with the content of the download.
	 */
    private String downloadAsString(String urlString) throws IOException {
        HttpURLConnection connection = null;
        
        try {
            connection = connectToUrl(urlString);
        	InputStream downloadStream = new BufferedInputStream(connection.getInputStream());
        	
            Reader reader = new InputStreamReader(downloadStream, "UTF-8");
            final int BUFFER_SIZE = (256 * 1024) / 2; // 256 KiB buffer.
            char[] buffer = new char[BUFFER_SIZE];
            
            String htmlPage = "";
            
            int numCharsRead = reader.read(buffer, 0, BUFFER_SIZE);
            while (numCharsRead != -1) {
                htmlPage = htmlPage + new String(buffer);
                numCharsRead = reader.read(buffer, 0, BUFFER_SIZE);
            }
            
            return htmlPage;
        } catch (IOException e) {
            throw e;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
    
    /**
     * Downloads the URL and returns a {@link Bitmap} from it.
     * @param imageUrl The URL to download
     * @return Bitmap downloaded, or null if failed.
     */
    private Bitmap downloadAsBitmap(String imageUrl) throws IOException {
        HttpURLConnection connection = null;
        
        try {
            connection = connectToUrl(imageUrl);
            InputStream downloadStream = new BufferedInputStream(connection.getInputStream());
            return BitmapFactory.decodeStream(downloadStream);
        } catch (IOException e) {
            throw e;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    /**
     * Get all image URLs on a web page.
     * @param webPageContent The web page as a String.
     * @return A set with the URLs found on the web page. This can be empty.
     */
    private Set<String> getImageUrls(String webPageContent) {
    	Document webDocument = Jsoup.parse(webPageContent);
    	Elements imgElements = webDocument.select("img[src~=(?i)\\.(png|jpe?g|gif)]");
    	Log.d(TAG, "Found " + imgElements.size() + " images.\n");
    	
    	Set<String> urlList = new HashSet<String>();
    	
    	for (Element imgElement : imgElements) {
    		String url = imgElement.attr("abs:src");
    		if (url.startsWith("http://")) { // we only accept http urls.
    			urlList.add(url);
    		}
    	}
    	
    	return urlList;
    }

    /**
     * A task downloading a web page, finding all (most) images on it,
     * downloading them (or loading from cache), saving them, and displays them.
     * @author Magnus Bjerke Vik
     *
     */
    private class DownloadImagesTask extends AsyncTask<String, Image, DownloadImagesTask.CompletionState> {
        boolean firstImageDownloaded = false;
        
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            ImagesViewFragment.this.downloadingWebPage = true;
            showLoadingProgress();
        }

        @Override
        protected void onProgressUpdate(Image... values) {
    	    if (this.firstImageDownloaded == false) {
    	        ImagesViewFragment.this.downloadingWebPage = false;
    	        this.firstImageDownloaded = true;
    	        showImages();
    	    }
    	    
    		for (Image image : values) {
		        ImagesViewFragment.this.imageArrayAdapter.add(image);
		        new Thread(new SaveImageTask(image, ImagesViewFragment.this.dbHelper)).start();
    		}
        }
        
        @Override
        protected void onPostExecute(CompletionState state) {
            super.onPostExecute(state);
            showImages();
            
            if (state.succeeded() == false) {
                Toast errorToast = Toast.makeText(getActivity(), state.getError(), Toast.LENGTH_SHORT);
                errorToast.show();
            }
        }

        // TODO: This should be divided into functions.
        @Override
		protected CompletionState doInBackground(String... params) {
		    assert(params.length == 1);
		    Set<String> imageUrlList = null;
		    
		    // Download web page.
            try {
                String webPageContent = downloadAsString(params[0]);
                if (webPageContent != null && webPageContent.length() > 0) {
                    imageUrlList = getImageUrls(webPageContent);
                }
            } catch (IOException e) {
                e.printStackTrace();
                return new CompletionState().setFail("Failed to connect to " + ImagesViewFragment.this.webpageUrl.getUrl());
            }
            
            // Remove outdated images from storage.
            List<String> storedImages = Image.getAllImageUrls(ImagesViewFragment.this.webpageUrl, ImagesViewFragment.this.dbHelper);
            for (String storedImageUrl : storedImages) {
                boolean outdated = true;
                
                for (String downloadedImageUrl : imageUrlList) {
                    if (storedImageUrl.equals(downloadedImageUrl)) {
                        outdated = false;
                        break;
                    }
                }
                
                if (outdated == true) {
                    Log.d(TAG, "Removing " + storedImageUrl);
                    Image outdatedImage = Image.get(storedImageUrl, ImagesViewFragment.this.dbHelper, ImagesViewFragment.this.applicationContext);
                    outdatedImage.remove(ImagesViewFragment.this.dbHelper);
                }
            }
            
            // No images found.
            if (imageUrlList.isEmpty()) {
                return new CompletionState().setFail("No images were found.");
            }
            
            // Get each image
            for (String imageUrl : imageUrlList) {
                if (isCancelled()) {
                    return new CompletionState().setFail("Download was interrupted.");
                }
                
                Image image = Image.get(imageUrl, ImagesViewFragment.this.dbHelper, ImagesViewFragment.this.applicationContext);
                
                // Try to load from storage.
                if (image != null) {
                    try {
                        Log.d(TAG, "Loading " + image.getFileName());
                        image.loadBitmap();
                    } catch (IOException e1) {
                        Log.d(TAG, "Loading failed");
                        e1.printStackTrace();
                        image = null;
                    }
                }
                
                // Try to download from interwebs if not loaded from storage.
                if (image == null) {
                    Bitmap bitmap = null;
                    
                    try {
                        Log.d(TAG, "Downloading " + imageUrl);
                        bitmap = downloadAsBitmap(imageUrl);
                    } catch (IOException e) {
                        Log.d(TAG, "Downloading failed");
                        e.printStackTrace();
                    }
                    
                    if (bitmap != null) {
                        image = new Image(ImagesViewFragment.this.webpageUrl, bitmap, imageUrl, ImagesViewFragment.this.applicationContext);
                    }
                }
                
                if (image != null && isCancelled() == false) {
                    publishProgress(image);
                }
            }
			
			return new CompletionState().setSuccess();
		}
        
        private class CompletionState {
            private boolean succeeded = false;
            private String error = null;
            
            public String getError() {
                return this.error;
            }

            public boolean succeeded() {
                return this.succeeded;
            }
            
            public CompletionState setFail(String error) {
                this.succeeded = false;
                this.error = error;
                return this;
            }
            
            public CompletionState setSuccess() {
                this.succeeded = true;
                return this;
            }
        }
    }
    
    /**
     * Load already existing images fetched from the UrlFetch provided.
     * @author Magnus Bjerke Vik
     *
     */
    private class LoadImagesTask extends AsyncTask<UrlFetch, Image, Void> {
        @Override
        protected void onProgressUpdate(Image... values) {
            for (Image image : values) {
                ImagesViewFragment.this.imageArrayAdapter.add(image);
            }
        }
        
        @Override
        protected Void doInBackground(UrlFetch... params) {
            assert(params.length == 1);
            UrlFetch urlFetch = params[0];
            
            List<Image> imageList = Image.getAll(urlFetch, ImagesViewFragment.this.dbHelper, ImagesViewFragment.this.applicationContext);
            
            for (Image image : imageList) {
                try {
                    image.loadBitmap();
                    
                    if (isCancelled()) {
                        return null;
                    }
                    
                    publishProgress(image);
                    Log.d(TAG, "Loaded already existing image " + image.getUrl());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            
            return null;
        }
    }
    
    /**
     * Save {@link Image} {@link Bitmap} to storage.
     * @author Magnus Bjerke Vik
     */
    private class SaveImageTask implements Runnable {
        private Image image;
        private SQLiteOpenHelper dbHelper;
        
        /**
         * @param image Image to save.
         */
        public SaveImageTask(Image image, SQLiteOpenHelper dbHelper) {
            this.image = image;
            this.dbHelper = dbHelper;
        }
        
        @Override
        public void run() {
            try {
                Log.d(TAG, "Saving " + image.getFileName());
                this.image.save(this.dbHelper); 
            } catch (IOException e) {
                Log.d(TAG, "Saving failed");
                e.printStackTrace();
            }
        }
        
    }
}
