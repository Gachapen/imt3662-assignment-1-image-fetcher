package com.gachapen.hig.mobile.imagefetch;

import java.util.Calendar;
import java.util.List;

import com.gachapen.hig.mobile.imagefetch.R;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

/**
 * Presents the input box and button to the user and handles auto completion
 * of inputs and forwards button clicks to the activity.
 * @author Magnus Bjerke Vik
 */
public class UrlInputFragment extends Fragment {
    private InputSetListener inputSetListener;
	private AutocompleteAdapter autoCompleteAdapter;
	private List<UrlFetch> urlFetchList;
	
	/**
	 * Attaches to the activity only if it implements the {@link InputSetListener}.
	 * If it doesn't, this function will throw a {@link ClassCastException}.
	 */
	@Override
	public void onAttach(Activity activity) throws ClassCastException {
	    super.onAttach(activity);
	    try {
	        this.inputSetListener = (InputSetListener) activity;
	    } catch (ClassCastException e) {
	        throw new ClassCastException(activity.toString() + " must implement InputSetListener");
	    }
	}

	/**
	 * Loads the {@link UrlFetch} instances from the database and adds these to the {@link AutocompleteAdapter}
	 * so that they will show when the user types.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        this.urlFetchList = UrlFetch.getAll(new DatabaseHelper(getActivity()));
        this.autoCompleteAdapter = new AutocompleteAdapter(getActivity(), android.R.layout.simple_list_item_1, urlFetchList);
	}
	
	/**
	 * Inflates the view from the xml, and sets listeners so that when the user presses the button or
	 * the go key on the keyboard, inputSet() will be called. Also sets the latest UrlFetch to
	 * be displayed in the text box.
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	    final View view = inflater.inflate(R.layout.fragment_url_input, container, false);
	    
	    final Button inputButton = (Button) view.findViewById(R.id.url_fetch_btn);
	    inputButton.setOnClickListener(new View.OnClickListener() {
	        @Override
	        public void onClick(View v) {
	            inputSet();
	        }
	    });
	    
	    final AutoCompleteTextView inputText = (AutoCompleteTextView) view.findViewById(R.id.url_input);
	    inputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
	        @Override
	        public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
	            inputSet();
	            return true;
	        }
	    });
	    
	    inputText.setAdapter(this.autoCompleteAdapter);
	    
        UrlFetch webpageUrl = UrlFetch.getLatest(new DatabaseHelper(getActivity()));
        if (webpageUrl != null) {
            inputText.setText(webpageUrl.getUrl());
        }
	    
	    return view;
	}
	
	@Override
	public void onDestroy() {
	    super.onDestroy();
	}
	
	/**
	 * Updates the UrlFetch list. If the new URL already is in the list, it will be moved
	 * to the front of the list and will get its time updated to now. If it is not already
	 * in the list, it will get its time set to now and inserted. It will then be saved
	 * to the database.
	 * @param newUrl The new URL to add to the list.
	 * @return The UrlFetch created or changed.
	 */
	private UrlFetch updateUrlFetches(String newUrl) {
	    UrlFetch affectedUrlFetch = null;
	    
	    boolean fetchAlreadyExists = false;
	    for (UrlFetch urlFetch : this.urlFetchList) {
	        if (urlFetch.getUrl().equals(newUrl)) {
	            fetchAlreadyExists = true;
	            urlFetch.updateToCurrentTime();
	            this.urlFetchList.remove(urlFetch);
	            this.urlFetchList.add(0, urlFetch);
	            affectedUrlFetch = urlFetch;
	            break;
	        }
	    }
	    
	    if (fetchAlreadyExists == false) {
	        affectedUrlFetch = new UrlFetch(newUrl, Calendar.getInstance().getTime());
	        this.urlFetchList.add(0, affectedUrlFetch);
	    }
	    
        DatabaseHelper dbHelper = new DatabaseHelper(getActivity());
        affectedUrlFetch.save(dbHelper);
	    this.autoCompleteAdapter.notifyDataSetChanged();
	    return affectedUrlFetch;
	}
	
	/**
	 * Called when the input is set by the user. This will update the UrlFetch list and notify the activity
	 * about the input.
	 */
	private void inputSet() {
	    AutoCompleteTextView inputEdit = (AutoCompleteTextView) getView().findViewById(R.id.url_input);
	    String inputString = inputEdit.getText().toString();
        if (inputString.contains("://") == false) {
            inputString = "http://" + inputString;
        }
	    
	    inputEdit.clearFocus();
	    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
	    imm.hideSoftInputFromWindow(inputEdit.getWindowToken(), 0);
	    
	    UrlFetch urlFetch = updateUrlFetches(inputString);
	    
	    this.inputSetListener.onInputSet(urlFetch); 
	}
	
	/**
	 * An interface for listening to the input set in this fragment.
	 * @author Magnus Bjerke Vik
	 */
	public interface InputSetListener {
		public void onInputSet(UrlFetch urlFetch);
	}
}
