package com.gachapen.hig.mobile.imagefetch;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * A class representing a fetch of a URL.
 * This class is used to store information about when and what URL was used
 * and can be stored in a database.
 * @author Magnus Bjerke Vik
 */
class UrlFetch {
    private long id;
    private String url;
    
    /** The time this URL was last used by the user. */
    private Date lastUsed;
    
    public static final String TABLE_NAME = "url_fetch";
    private static final String COLUMN_ID = "id";
    private static final String COLUMN_URL = "url";
    private static final String COLUMN_TIME_USED = "time_used";
    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
                                                + COLUMN_ID + " INTEGER PRIMARY KEY,"
                                                + COLUMN_URL + " TEXT,"
                                                + COLUMN_TIME_USED + " INTEGER"
                                                + ");";
    
    public UrlFetch(String value, Date timeLastUsed) {
        this.id = -1;
        this.url = value;
        this.lastUsed = timeLastUsed;
    }
    
    public long getId() {
        return this.id;
    }
    
    public String getUrl() {
        return this.url;
    }
    
    public Date getLastTimeUsed() {
        return this.lastUsed;
    }
    
    /**
     * Updates this instance's last time used to the current tim.
     */
    public void updateToCurrentTime() {
        this.lastUsed = Calendar.getInstance().getTime();
    }
    
    public String toString() {
        return getUrl();
    }
    
    /**
     * Save this instance to the database.
     */
    public void save(SQLiteOpenHelper dbHelper) {
        final boolean alreadInDatabase = (this.id > -1);
        if (alreadInDatabase) {
            updateInDb(dbHelper);
        } else {
            insertInDb(dbHelper);
        }
    }
    
    /**
     * Get a specific UrlFetch having the provided id.
     * @param id The id of the fetch to get.
     * @return The first UrlFetch having the id, or null.
     */
    public static UrlFetch get(final long id, final SQLiteOpenHelper dbHelper) {
        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        final String whereClause = "" + COLUMN_ID + " = " + id;
        final Cursor entryCursor = db.query(
                TABLE_NAME, 
                new String[] { COLUMN_ID, COLUMN_URL, COLUMN_TIME_USED }, 
                whereClause, 
                null, 
                null, 
                null, 
                null
        );
        
        UrlFetch value = null;
        
        entryCursor.moveToFirst();
        if (!entryCursor.isAfterLast()) {
            value = cursorToUrlFetch(entryCursor);
        }
        
        entryCursor.close();
        db.close();
        
        return value;
    }
    
    /**
     * Get the latest UrlFetch from the database based on last time used.
     * @return The latest UrlFetch, or null.
     */
    public static UrlFetch getLatest(final SQLiteOpenHelper dbHelper) {
        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        final Cursor entryCursor = db.query(
                TABLE_NAME, 
                new String[] { COLUMN_ID, COLUMN_URL, "MAX(" + COLUMN_TIME_USED + ") AS " + COLUMN_TIME_USED }, 
                null, 
                null, 
                null, 
                null, 
                null
        );
        
        UrlFetch value = null;
        
        entryCursor.moveToFirst();
        if (!entryCursor.isAfterLast()) {
            value = cursorToUrlFetch(entryCursor);
        }
        
        entryCursor.close();
        db.close();
        
        return value;
    }
    
    /**
     * Get a list of all the UrlFetchs in the database.
     * @return A list of all the UrlFetch's in the database. This can be empty.
     */
    public static List<UrlFetch> getAll(final SQLiteOpenHelper dbHelper) {
        final List<UrlFetch> autocompleteList = new ArrayList<UrlFetch>();
        final SQLiteDatabase db = dbHelper.getReadableDatabase();
        final Cursor entryCursor = db.query(
                TABLE_NAME, 
                new String[] { COLUMN_ID, COLUMN_URL, COLUMN_TIME_USED }, 
                null, 
                null, 
                null, 
                null, 
                COLUMN_TIME_USED + " DESC"
        );
        
        entryCursor.moveToFirst();
        while (!entryCursor.isAfterLast()) {
            final UrlFetch value = cursorToUrlFetch(entryCursor);
            Log.d(UrlFetch.class.getName(), "Loading autocomplete value " + value.getUrl());
            autocompleteList.add(value);
            entryCursor.moveToNext();
        }
        
        entryCursor.close();
        db.close();
        
        return autocompleteList;
    }
    
    /**
     * Insert this instance's values as a new row into the DB.
     */
    private void insertInDb(SQLiteOpenHelper dbHelper) {
        final ContentValues values = new ContentValues();
        values.put(COLUMN_URL, this.url);
        values.put(COLUMN_TIME_USED, this.lastUsed.getTime());
        
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        this.id = db.insert(TABLE_NAME, null, values);
        Log.d(this.toString(), "Inserting " + this.url + " " + this.lastUsed.getTime() + " into " + TABLE_NAME);
        db.close();
    }
    
    /**
     * Update this instance's value in its existing row if it has one.
     * @return true if updated. false if it is not already in the DB.
     */
    private boolean updateInDb(SQLiteOpenHelper dbHelper) {
        if (this.id < 0) {
            return false;
        }
        
        final ContentValues values = new ContentValues();
        values.put(COLUMN_TIME_USED, this.lastUsed.getTime());
        final String whereClause = "id = " + this.id;
        
        final SQLiteDatabase db = dbHelper.getWritableDatabase();
        final int numRowsUpdated = db.update(TABLE_NAME, values, whereClause, null);
        if (numRowsUpdated != 1) {
            db.close();
            Log.e(UrlFetch.class.getName(), "Updated incorrect number of rows");
            return false;
        }
        
        db.close();
        
        return true;
    }
    
    /**
     * Converts a Cursor to a UrlFech instance.
     * @param cursor The cursor to convert.
     * @return The converted UrlFetch containing the values from the Cursor.
     */
    private static UrlFetch cursorToUrlFetch(Cursor cursor) {
        final long id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
        final String value = cursor.getString(cursor.getColumnIndex(COLUMN_URL));
        final Date time = new Date(cursor.getLong(cursor.getColumnIndex(COLUMN_TIME_USED)));
        
        final UrlFetch autocomplete = new UrlFetch(value, time);
        autocomplete.id = id;
        
        return autocomplete;
    }
}

